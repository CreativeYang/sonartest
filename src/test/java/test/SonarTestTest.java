package test;

import org.junit.Test;

import static org.junit.Assert.*;

public class SonarTestTest {

    @Test
    public void sum() {
        SonarTest sonarTest = new SonarTest();
        int a = 8;
        int b = 10;
        int expectResult = 18;
        int sum = sonarTest.sum(a, b);
        assertEquals(expectResult,sum);
    }
}